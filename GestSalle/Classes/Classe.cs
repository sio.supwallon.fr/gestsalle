﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Classe
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public static List<Classe> GetAll() => ClasseDao.GetAll();
        public static Classe Get(int id) => ClasseDao.Get(id);
    }
}
