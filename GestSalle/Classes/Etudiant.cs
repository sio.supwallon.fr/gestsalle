﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Etudiant : IComparable
    {
        public int Id { get; set; }
        public string Prénom { get; set; }
        public string Nom { get; set; }
        public string NomComplet { get => Prénom + " " + Nom; }
        public Classe Classe { get; set; }
        private List<Groupe> groupes;
        public List<Groupe> Groupes
        {
            get
            {
                if(groupes == null)
                {
                    groupes = Groupe.GetAll(Id);
                }
                return groupes;
            }
        }

        public static List<Etudiant> GetAll(int id_groupe) => EtudiantDao.GetAll(id_groupe);
        public static Etudiant Get(Placement placement) => EtudiantDao.Get(placement);

        public int CompareTo(object obj)
        {
            int result;

            if (obj == null)
            {
                result = 1;
            }

            if (obj is Etudiant other)
                result = Nom.CompareTo(other.Nom) * 1000 + Prénom.CompareTo(other.Prénom);
            else
                throw new ArgumentException("Compare un étudiant avec un objet qui n'en est pas un.");

            return result;
        }

        public override bool Equals(object obj)
        {
            bool res = true;
            if (obj == null)
            {
                res = false;
            }
            else if(! (obj is Etudiant etudiant))
            {
                res = false;
            }
            else
            {
                res = this.Id == etudiant.Id;
            }
            return res;
        }
    }
}
