﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Dao;

namespace GestSalle.Classes
{
    public class Placement
    {
        public Groupe Groupe { get; set; }
        public Salle Salle { get; set; }
        public Poste Poste { get; set; }

        public static void Save(Groupe groupe, Salle salle, Dictionary<Poste, Etudiant> placements) => PlacementDao.Save(groupe, salle, placements);
    }
}
