﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class ClasseDao
    {
        public static List<Classe> GetAll()
        {
            List<Classe> items = new List<Classe>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `classe`";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Classe
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }

        public static Classe Get(int id)
        {
            Classe item = null;
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * FROM `classe` WHERE id=?id";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", id));
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                item = new Classe
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                };
            }
            reader.Close();
            connection.Close();
            return item;
        }
    }
}
