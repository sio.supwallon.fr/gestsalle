﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class EtudiantDao
    {
        public static List<Etudiant> GetAll(int id_groupe)
        {
            List<Etudiant> items = new List<Etudiant>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT * " +
                            "FROM `etudiant` " +
                            "INNER JOIN `etudiant_groupe` ON `etudiant`.`id` = `etudiant_groupe`.`id_etudiant` " +
                            "WHERE `id_groupe` = ?id_groupe;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", id_groupe));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Etudiant
                {
                    Id = (int)reader["id"],
                    Prénom = (string)reader["prenom"],
                    Nom = (string)reader["nom"],
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }

        public static Etudiant Get(Placement placement)
        {
            Etudiant item = null;
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query = "SELECT `etudiant`.* " +
                            "FROM `etudiant` " +
                            "INNER JOIN `groupe_salle_poste` ON `groupe_salle_poste`.`id_etudiant` = `etudiant`.`id` " +
                            "WHERE `id_groupe` = ?id_groupe " +
                            "AND `id_salle` = ?id_salle " +
                            "AND `id_poste` = ?id_poste;";
            MySqlCommand command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", placement.Groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", placement.Salle.Id));
            command.Parameters.Add(new MySqlParameter("id_poste", placement.Poste.Id));
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                item = new Etudiant
                {
                    Id = (int)reader["id"],
                    Prénom = (string)reader["prenom"],
                    Nom = (string)reader["nom"]
                };
            }
            reader.Close();
            connection.Close();
            return item;
        }
    }
}