﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestSalle.Classes;
using GestSalle.Dal;

namespace GestSalle.Dao
{
    public static class PlacementDao
    {
        public static void Save(Groupe groupe, Salle salle, Dictionary<Poste, Etudiant> placements)
        {
            List<Etudiant> items = new List<Etudiant>();
            MySqlConnection connection = AbstractDao.Connection;
            connection.Open();
            string query;
            MySqlCommand command;
            int nbRows;

            query = "DELETE FROM `groupe_salle_poste` WHERE `id_groupe` = ?id_groupe AND `id_salle` = ?id_salle;";
            command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", salle.Id));
            nbRows = command.ExecuteNonQuery();

            query = "INSERT INTO `groupe_salle_poste` " +
                    "(`id_groupe`, `id_salle`, `id_poste`, `id_etudiant`)" +
                    "VALUES ";

            for (int i = 0; i < placements.Count; i++)
            {
                query += $"(?id_groupe, ?id_salle, ?id_poste_{i}, ?id_etudiant_{i}),\n";
            }
            query = query.Substring(0, query.Length -2);

            command = AbstractDao.Connection.CreateCommand();
            command.CommandText = query;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id_groupe", groupe.Id));
            command.Parameters.Add(new MySqlParameter("id_salle", salle.Id));

            List<Poste> postes = placements.Keys.ToList();
            for (int i = 0; i < placements.Count; i++)
            {
                Poste poste = postes[i];
                Etudiant etudiant = placements[poste];
                command.Parameters.Add(new MySqlParameter($"id_poste_{i}", poste.Id));
                command.Parameters.Add(new MySqlParameter($"id_etudiant_{i}", etudiant.Id));
            }
            nbRows = command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
