-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 09 mai 2019 à 15:46
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gest_salle`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `nom`) VALUES
(1, 'SIO1'),
(2, 'SIO2');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`id`, `prenom`, `nom`, `id_classe`) VALUES
(1, 'Marion', 'Alexandre', 1),
(2, 'Benjamin', 'Bailly', 1),
(3, 'Jordane', 'Becard', 1),
(4, 'Mathieu', 'Bera', 1),
(5, 'Eden', 'Blervacque', 1),
(6, 'Alan', 'Boucaut', 1),
(7, 'Amazigh', 'Brahmi', 1),
(8, 'Thibault', 'Choain', 1),
(9, 'Paul', 'Coelho', 1),
(10, 'Alexandre', 'Demarcq', 1),
(11, 'Thomas', 'Dhollande', 1),
(12, 'Nathan', 'Dierickx', 1),
(13, 'Axel', 'Dubrulle', 1),
(14, 'Thomas', 'Duhain-Mercier', 1),
(15, 'Rodrigue', 'Dumez', 1),
(16, 'Maxime', 'Felder', 1),
(17, 'Antoine', 'Florin', 1),
(18, 'Célia', 'Garmyn', 1),
(19, 'Maxime', 'Gobeau', 1),
(20, 'Dorian', 'Grattepanche', 1),
(21, 'Sébastien', 'Gricourt', 1),
(22, 'Julien', 'Holin', 1),
(23, 'Florian', 'Hourdin', 1),
(24, 'Bastien', 'Hus', 1),
(25, 'Tristan', 'Lanoy', 1),
(26, 'Alexis', 'Leger', 1),
(27, 'Guillaume', 'Lepetz', 1),
(28, 'Thomas', 'Lepillez', 1),
(29, 'Quentin', 'Marcaille', 1),
(30, 'Maxime', 'Mazingue', 1),
(31, 'Nathan', 'Menneveux', 1),
(32, 'Pierre', 'Papillaud', 1),
(33, 'Stéphane', 'Poisson', 1),
(34, 'Quincy', 'Radojcic', 1),
(35, 'Thomas', 'Rocq', 1),
(36, 'Julien', 'Sabben', 1),
(37, 'Gauthier', 'Salvadori', 1),
(38, 'Augustin', 'Sergent', 1),
(39, 'Axel', 'Simon', 1),
(40, 'Thomas', 'Stremez', 1),
(41, 'Kevin', 'Tison', 1),
(42, 'Evrick', 'Tisserand', 1),
(43, 'Kevin', 'Vinoy', 1);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_groupe`
--

DROP TABLE IF EXISTS `etudiant_groupe`;
CREATE TABLE IF NOT EXISTS `etudiant_groupe` (
  `id_etudiant` int(11) NOT NULL,
  `id_groupe` int(11) NOT NULL,
  PRIMARY KEY (`id_etudiant`,`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant_groupe`
--

INSERT INTO `etudiant_groupe` (`id_etudiant`, `id_groupe`) VALUES
(1, 1),
(2, 1),
(2, 4),
(3, 1),
(3, 3),
(4, 1),
(4, 4),
(5, 1),
(5, 4),
(6, 1),
(7, 1),
(7, 4),
(8, 1),
(9, 1),
(9, 3),
(10, 1),
(10, 3),
(11, 1),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(13, 3),
(14, 1),
(14, 4),
(15, 1),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 4),
(18, 1),
(18, 4),
(19, 1),
(19, 4),
(20, 1),
(20, 4),
(21, 1),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 4),
(25, 1),
(26, 1),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 4),
(30, 1),
(30, 4),
(31, 1),
(31, 4),
(32, 1),
(33, 1),
(33, 4),
(34, 1),
(34, 4),
(35, 1),
(36, 1),
(37, 1),
(37, 4),
(38, 1),
(38, 4),
(39, 1),
(40, 1),
(41, 1),
(41, 3),
(42, 1),
(42, 4),
(43, 1),
(43, 3);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'SIO1'),
(2, 'SIO2'),
(3, 'SIO1-SLAM'),
(4, 'SIO1-SISR'),
(5, 'SIO2-SLAM'),
(6, 'SIO2-SISR');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_salle_poste`
--

DROP TABLE IF EXISTS `groupe_salle_poste`;
CREATE TABLE IF NOT EXISTS `groupe_salle_poste` (
  `id_groupe` int(11) NOT NULL,
  `id_salle` int(11) NOT NULL,
  `id_poste` int(11) NOT NULL,
  `id_etudiant` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_salle`,`id_poste`),
  KEY `id_etudiant` (`id_etudiant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe_salle_poste`
--

INSERT INTO `groupe_salle_poste` (`id_groupe`, `id_salle`, `id_poste`, `id_etudiant`) VALUES
(3, 1, 16, 10),
(3, 1, 15, 12),
(3, 1, 14, 16),
(3, 1, 13, 28),
(3, 1, 12, 3),
(3, 1, 11, 26),
(3, 1, 8, 27),
(3, 1, 6, 22),
(3, 1, 5, 23),
(3, 1, 4, 11),
(3, 1, 2, 15);

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

DROP TABLE IF EXISTS `poste`;
CREATE TABLE IF NOT EXISTS `poste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(7) NOT NULL,
  `id_salle` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_salle` (`id_salle`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `poste`
--

INSERT INTO `poste` (`id`, `nom`, `id_salle`) VALUES
(1, 'S222P01', 1),
(2, 'S222P02', 1),
(3, 'S222P03', 1),
(4, 'S222P04', 1),
(5, 'S222P05', 1),
(6, 'S222P06', 1),
(7, 'S222P07', 1),
(8, 'S222P08', 1),
(9, 'S222P09', 1),
(10, 'S222P10', 1),
(11, 'S222P11', 1),
(12, 'S222P12', 1),
(13, 'S222P13', 1),
(14, 'S222P14', 1),
(15, 'S222P15', 1),
(16, 'S222P16', 1),
(17, 'S222P17', 1),
(18, 'S222P18', 1),
(19, 'S222P19', 1),
(20, 'S222P20', 1),
(21, 'S222P21', 1),
(22, 'S222P22', 1),
(23, 'S224P01', 2),
(24, 'S224P02', 2),
(25, 'S224P03', 2),
(26, 'S224P04', 2),
(27, 'S224P05', 2),
(28, 'S224P06', 2),
(29, 'S224P07', 2),
(30, 'S224P08', 2),
(31, 'S224P09', 2),
(32, 'S224P10', 2),
(33, 'S224P11', 2),
(34, 'S224P12', 2),
(35, 'S224P13', 2),
(36, 'S224P14', 2),
(37, 'S224P15', 2),
(38, 'S224P16', 2),
(39, 'S224P17', 2),
(40, 'S224P18', 2),
(41, 'S224P19', 2),
(42, 'S224P20', 2),
(43, 'S224P21', 2),
(44, 'S224P22', 2),
(45, 'S227P01', 3),
(46, 'S227P02', 3),
(47, 'S227P03', 3),
(48, 'S227P04', 3),
(49, 'S227P05', 3),
(50, 'S227P06', 3),
(51, 'S227P07', 3),
(52, 'S227P08', 3),
(53, 'S227P09', 3),
(54, 'S227P10', 3),
(55, 'S227P11', 3),
(56, 'S227P12', 3),
(57, 'S227P13', 3),
(58, 'S227P14', 3),
(59, 'S227P15', 3),
(60, 'S227P16', 3),
(61, 'S227P17', 3),
(62, 'S227P18', 3),
(63, 'S227P19', 3),
(64, 'S227P20', 3),
(65, 'S227P21', 3),
(66, 'S227P22', 3);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `nom`) VALUES
(1, '222'),
(2, '224'),
(3, '227');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
