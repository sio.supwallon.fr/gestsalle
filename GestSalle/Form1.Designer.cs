﻿namespace GestSalle
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.lstEtudiants = new System.Windows.Forms.ListBox();
            this.cmbSalles = new System.Windows.Forms.ComboBox();
            this.lblSalle = new System.Windows.Forms.Label();
            this.lblGroupe = new System.Windows.Forms.Label();
            this.cmbGroupes = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.Panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.groupBox4);
            this.Panel1.Controls.Add(this.groupBox3);
            this.Panel1.Controls.Add(this.groupBox2);
            this.Panel1.Controls.Add(this.groupBox1);
            this.Panel1.Location = new System.Drawing.Point(242, 218);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(174, 174);
            this.Panel1.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(89, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(80, 80);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "P04";
            this.groupBox4.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox4.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(89, 89);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(80, 80);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "P03";
            this.groupBox3.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox3.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(80, 80);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "P02";
            this.groupBox2.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox2.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(3, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(80, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "P01";
            this.groupBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Location = new System.Drawing.Point(242, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(174, 174);
            this.panel2.TabIndex = 1;
            // 
            // groupBox8
            // 
            this.groupBox8.Location = new System.Drawing.Point(89, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(80, 80);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "P08";
            this.groupBox8.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox8.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox7
            // 
            this.groupBox7.Location = new System.Drawing.Point(89, 89);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(80, 80);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "P07";
            this.groupBox7.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox7.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox6
            // 
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(80, 80);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "P06";
            this.groupBox6.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox6.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(3, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(80, 80);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "P05";
            this.groupBox5.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox5.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.groupBox16);
            this.panel3.Controls.Add(this.groupBox15);
            this.panel3.Controls.Add(this.groupBox14);
            this.panel3.Controls.Add(this.groupBox13);
            this.panel3.Location = new System.Drawing.Point(448, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 174);
            this.panel3.TabIndex = 3;
            // 
            // groupBox16
            // 
            this.groupBox16.Location = new System.Drawing.Point(89, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(80, 80);
            this.groupBox16.TabIndex = 4;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "P16";
            this.groupBox16.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox16.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox15
            // 
            this.groupBox15.Location = new System.Drawing.Point(89, 89);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(80, 80);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "P15";
            this.groupBox15.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox15.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox14
            // 
            this.groupBox14.Location = new System.Drawing.Point(3, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(80, 80);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "P14";
            this.groupBox14.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox14.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox13
            // 
            this.groupBox13.Location = new System.Drawing.Point(3, 89);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(80, 80);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "P13";
            this.groupBox13.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox13.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.groupBox12);
            this.panel4.Controls.Add(this.groupBox11);
            this.panel4.Controls.Add(this.groupBox10);
            this.panel4.Controls.Add(this.groupBox9);
            this.panel4.Location = new System.Drawing.Point(448, 218);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(174, 174);
            this.panel4.TabIndex = 2;
            // 
            // groupBox12
            // 
            this.groupBox12.Location = new System.Drawing.Point(89, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(80, 80);
            this.groupBox12.TabIndex = 4;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "P12";
            this.groupBox12.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox12.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox11
            // 
            this.groupBox11.Location = new System.Drawing.Point(89, 89);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(80, 80);
            this.groupBox11.TabIndex = 3;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "P11";
            this.groupBox11.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox11.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox10
            // 
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(80, 80);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "P10";
            this.groupBox10.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox10.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox9
            // 
            this.groupBox9.Location = new System.Drawing.Point(3, 89);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(80, 80);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "P09";
            this.groupBox9.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox9.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.groupBox24);
            this.panel5.Controls.Add(this.groupBox23);
            this.panel5.Controls.Add(this.groupBox22);
            this.panel5.Controls.Add(this.groupBox21);
            this.panel5.Location = new System.Drawing.Point(654, 12);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(174, 174);
            this.panel5.TabIndex = 5;
            // 
            // groupBox24
            // 
            this.groupBox24.Location = new System.Drawing.Point(89, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(80, 80);
            this.groupBox24.TabIndex = 4;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "P24";
            this.groupBox24.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox24.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox23
            // 
            this.groupBox23.Location = new System.Drawing.Point(89, 89);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(80, 80);
            this.groupBox23.TabIndex = 3;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "P23";
            this.groupBox23.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox23.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox22
            // 
            this.groupBox22.Location = new System.Drawing.Point(3, 3);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(80, 80);
            this.groupBox22.TabIndex = 2;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "P22";
            this.groupBox22.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox22.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox21
            // 
            this.groupBox21.Location = new System.Drawing.Point(3, 89);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(80, 80);
            this.groupBox21.TabIndex = 2;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "P21";
            this.groupBox21.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox21.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.groupBox20);
            this.panel6.Controls.Add(this.groupBox19);
            this.panel6.Controls.Add(this.groupBox18);
            this.panel6.Controls.Add(this.groupBox17);
            this.panel6.Location = new System.Drawing.Point(654, 218);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(174, 174);
            this.panel6.TabIndex = 4;
            // 
            // groupBox20
            // 
            this.groupBox20.Location = new System.Drawing.Point(89, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(80, 80);
            this.groupBox20.TabIndex = 4;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "P20";
            this.groupBox20.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox20.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox19
            // 
            this.groupBox19.Location = new System.Drawing.Point(89, 89);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(80, 80);
            this.groupBox19.TabIndex = 3;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "P19";
            this.groupBox19.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox19.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox18
            // 
            this.groupBox18.Location = new System.Drawing.Point(3, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(80, 80);
            this.groupBox18.TabIndex = 2;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "P18";
            this.groupBox18.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox18.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // groupBox17
            // 
            this.groupBox17.Location = new System.Drawing.Point(3, 89);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(80, 80);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "P17";
            this.groupBox17.DragDrop += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapDrop);
            this.groupBox17.DragEnter += new System.Windows.Forms.DragEventHandler(this.GroupBox_DrapEnter);
            // 
            // lstEtudiants
            // 
            this.lstEtudiants.AllowDrop = true;
            this.lstEtudiants.FormattingEnabled = true;
            this.lstEtudiants.Location = new System.Drawing.Point(12, 64);
            this.lstEtudiants.Name = "lstEtudiants";
            this.lstEtudiants.Size = new System.Drawing.Size(224, 355);
            this.lstEtudiants.TabIndex = 6;
            this.lstEtudiants.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstEtudiants_DragDrop);
            this.lstEtudiants.DragEnter += new System.Windows.Forms.DragEventHandler(this.lstEtudiants_DragEnter);
            this.lstEtudiants.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lstEtudiants_MouseDown);
            // 
            // cmbSalles
            // 
            this.cmbSalles.FormattingEnabled = true;
            this.cmbSalles.Location = new System.Drawing.Point(115, 12);
            this.cmbSalles.Name = "cmbSalles";
            this.cmbSalles.Size = new System.Drawing.Size(121, 21);
            this.cmbSalles.TabIndex = 7;
            // 
            // lblSalle
            // 
            this.lblSalle.AutoSize = true;
            this.lblSalle.Location = new System.Drawing.Point(12, 15);
            this.lblSalle.Name = "lblSalle";
            this.lblSalle.Size = new System.Drawing.Size(36, 13);
            this.lblSalle.TabIndex = 8;
            this.lblSalle.Text = "Salle :";
            // 
            // lblGroupe
            // 
            this.lblGroupe.AutoSize = true;
            this.lblGroupe.Location = new System.Drawing.Point(9, 42);
            this.lblGroupe.Name = "lblGroupe";
            this.lblGroupe.Size = new System.Drawing.Size(48, 13);
            this.lblGroupe.TabIndex = 9;
            this.lblGroupe.Text = "Groupe :";
            // 
            // cmbGroupes
            // 
            this.cmbGroupes.FormattingEnabled = true;
            this.cmbGroupes.Location = new System.Drawing.Point(115, 39);
            this.cmbGroupes.Name = "cmbGroupes";
            this.cmbGroupes.Size = new System.Drawing.Size(121, 21);
            this.cmbGroupes.TabIndex = 10;
            this.cmbGroupes.SelectedIndexChanged += new System.EventHandler(this.cmbGroupes_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(753, 398);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Enregistrer";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(242, 398);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "R-à-Z";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 433);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbGroupes);
            this.Controls.Add(this.lblGroupe);
            this.Controls.Add(this.lblSalle);
            this.Controls.Add(this.cmbSalles);
            this.Controls.Add(this.lstEtudiants);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.ListBox lstEtudiants;
        private System.Windows.Forms.ComboBox cmbSalles;
        private System.Windows.Forms.Label lblSalle;
        private System.Windows.Forms.Label lblGroupe;
        private System.Windows.Forms.ComboBox cmbGroupes;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
    }
}

