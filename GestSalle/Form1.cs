﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestSalle.Classes;

namespace GestSalle
{
    public partial class Form1 : Form
    {
        private List<Salle> salles;
        private List<Groupe> groupes;
        private List<Etudiant> etudiants;
        private List<Poste> postes;
        private List<GroupBox> groupBoxes = new List<GroupBox>();

        private BindingSource bsSalles = new BindingSource();
        private BindingSource bsGroupes = new BindingSource();
        private BindingSource bsEtudiants = new BindingSource();

        public Form1()
        {
            InitializeComponent();
            GroupBoxInit();
        }

        private void GroupBoxInit()
        {
            foreach(Control control in this.Controls)
            {
                if (control is Panel)
                {
                    foreach(Control innerControl in control.Controls)
                    {
                        if (innerControl is GroupBox groupBox)
                        {
                            int index = int.Parse(groupBox.Name.Substring("groupBox".Length));
                            groupBox.AllowDrop = true;
                            groupBoxes.Add(groupBox);
                        }
                    }
                }
            }
            groupBoxes = groupBoxes.OrderBy(g => g.Text).ToList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            salles = Salle.GetAll();
            groupes = Groupe.GetAll();

            cmbSalles.DataSource = bsSalles;
            bsSalles.DataSource = salles;
            cmbSalles.DisplayMember = "Nom";

            cmbGroupes.DataSource = bsGroupes;
            bsGroupes.DataSource = groupes;
            cmbGroupes.DisplayMember = "Nom";

            lstEtudiants.DataSource = bsEtudiants;
            bsEtudiants.DataSource = etudiants;
            lstEtudiants.DisplayMember = "NomComplet";
            bsEtudiants.Sort = "Nom, Prénom";

            cmbSalles.SelectedIndexChanged += cmbSalles_SelectedIndexChanged;
            cmbSalles_SelectedIndexChanged(cmbSalles, EventArgs.Empty);
        }

        private void LoadEtudiantsFromGroupe()
        {
            etudiants = Etudiant.GetAll(((Groupe)cmbGroupes.SelectedValue).Id);
            bsEtudiants.DataSource = etudiants;
        }

        private void cmbGroupes_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEtudiantsFromGroupe();
            LoadPlacement();
        }

        private void lstEtudiants_MouseDown(object sender, MouseEventArgs e)
        {
            if (lstEtudiants.SelectedItem != null)
            {
                lstEtudiants.Tag = lstEtudiants.SelectedItem;
                DragDropEffects result = lstEtudiants.DoDragDrop(lstEtudiants, DragDropEffects.Move);
                if (result == DragDropEffects.Move)
                {
                    bsEtudiants.Remove(lstEtudiants.SelectedItem);
                }
            }

        }

        private void Label_MouseDown(object sender, MouseEventArgs e)
        {
            Label label = (Label)sender;
            if (label.Tag != null)
            {
                DragDropEffects result = label.DoDragDrop(label, DragDropEffects.Move);
                if (result == DragDropEffects.Move)
                {
                    label.Parent.Controls.Remove(label);
                }
            }
        }

        private void GroupBox_DrapEnter(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Control to = (Control)sender;
            if (from != null && from != to && from.Tag is Etudiant && to.Controls.Count == 0)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void lstEtudiants_DragEnter(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Control to = (Control)sender;
            if (from != null && from != to && from.Tag is Etudiant)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void GroupBox_DrapDrop(object sender, DragEventArgs e)
        {
            GroupBox groupBox = (GroupBox)sender;
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Etudiant etudiant = (Etudiant)from.Tag;
            AddEtudiantInGroupBox(etudiant, groupBox);
        }

        private void AddEtudiantInGroupBox(Etudiant etudiant, GroupBox groupBox)
        {
            Label label = new Label
            {
                AutoSize = false,
                BackColor = Color.Transparent,
                Location = new Point(3, 12),
                Parent = groupBox,
                Size = new Size(54, 54),
                Tag = etudiant,
                Text = etudiant.NomComplet
            };
            label.MouseDown += Label_MouseDown;
            groupBox.Controls.Add(label);
        }

        private void lstEtudiants_DragDrop(object sender, DragEventArgs e)
        {
            Control from = null;
            if (e.Data.GetDataPresent(typeof(ListBox)))
            {
                from = (Control)e.Data.GetData(typeof(ListBox));
            }
            else if (e.Data.GetDataPresent(typeof(Label)))
            {
                from = (Control)e.Data.GetData(typeof(Label));
            }
            Etudiant etudiant = (Etudiant)from.Tag;
            bsEtudiants.Add(etudiant);
            etudiants.Sort();
            bsEtudiants.ResetBindings(true);
        }

        private void cmbSalles_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEtudiantsFromGroupe();
            LoadPlacement();
        }

        private void LoadPlacement()
        {
            Salle salle = (Salle)cmbSalles.SelectedItem;
            Groupe groupe = (Groupe)cmbGroupes.SelectedItem;
            postes = Poste.GetAll(salle.Id);

            int index = 0;


            foreach (GroupBox groupBox in groupBoxes)
            {
                if (index < postes.Count)
                {
                    Poste poste = postes[index];
                    groupBox.Text = poste.Nom;
                    groupBox.Tag = poste;
                    int? id = Etudiant.Get(new Placement { Groupe = groupe, Salle = salle, Poste = poste })?.Id;
                    Etudiant etudiant = (id != null) ? etudiants.Find(etud => etud.Id == id) : null;
                    if (etudiant != null)
                    {
                        AddEtudiantInGroupBox(etudiant, groupBox);
                        bsEtudiants.Remove(etudiant);
                    }
                    else
                    {
                        groupBox.Controls.Clear();
                    }
                }
                else
                {
                    groupBox.Text = "";
                    groupBox.Tag = null;
                }
                index++;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Dictionary<Poste, Etudiant> emplacements = new Dictionary<Poste, Etudiant>();
            foreach(GroupBox groupBox in groupBoxes)
            {
                Poste poste = (Poste)groupBox.Tag;
                if (poste != null)
                {
                    if (groupBox.Controls.Count == 1)
                    {
                        Etudiant etudiant = (Etudiant)groupBox.Controls[0].Tag;
                        emplacements.Add(poste, etudiant);
                    }
                }
            }
            Placement.Save(new Groupe { Id = 3 }, new Salle { Id = 1 }, emplacements);
        }

        private void ResetPlacement()
        {
            foreach (GroupBox groupBox in groupBoxes)
            {
                groupBox.Controls.Clear();
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            LoadEtudiantsFromGroupe();
            ResetPlacement();
        }
    }
}
